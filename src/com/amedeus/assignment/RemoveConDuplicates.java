package com.amedeus.assignment;

import java.util.HashSet;
import java.util.Set;

public class RemoveConDuplicates {

    public String removeDuplicatesUsingRecursion(String str) {
        int i = 0;
        int j = 1;
        boolean duplicate = false;
        boolean repeat = false;
        StringBuilder output = new StringBuilder("");

        while (j < str.length()) {

            if (str.charAt(i) != str.charAt(j)) {
                if (duplicate) {
                    duplicate = false;
                } else {
                    output.append(str.charAt(i));
                }
                i = j;
                j++;
            } else {
                j++;
                duplicate = true;
            }

        }

        if (!duplicate) {
            output.append(str.charAt(str.length() - 1));
        }

        Set<Character> set = new HashSet<>();
        for (int k = 0; k < output.length(); k++) {
            if (!set.add(output.charAt(k))) {
                repeat = true;
                break;
            }
        }

        if (repeat) {
            return removeDuplicatesUsingRecursion(output.toString());
        } else {
            if (output.length() > 0) {
                return output.toString();
            } else {
                return "No Unique Element";
            }

        }
    }


    //abccbde
    public String removeDuplicatesLinearApproach(String str) {
        int i = 0;
        int j = 1;
        boolean duplicate = false;
        boolean removedLastFromOutput = false;

        String output = "";

        while (j < str.length()) {

            if (str.charAt(i) != str.charAt(j)) {

                if (!duplicate && !removedLastFromOutput) {
                    output = output + str.charAt(i);
                    i = j;
                    j++;
                }

                if (!duplicate && removedLastFromOutput) {
                    if (output.length() > 0 && output.charAt(output.length() - 1) == str.charAt(j)) {
                        output = output.substring(0, output.length() - 1);
                        i--;
                    } else {
                        output = output + str.charAt(j);
                        i = j;
                    }
                    j++;
                }


                if (duplicate) {


                    if (output.length() > 0 && str.charAt(j) == output.charAt(output.length() - 1)) {
                        output = output.substring(0, output.length() - 1);

                        removedLastFromOutput = true;
                    }

                    if (i == 0) {
                        i = j;

                    } else {
                        i--;

                    }
                    j++;
                    duplicate = false;
                }
            } else {
                duplicate = true;
                j++;
            }

        }

        /*if (!duplicate) {
            output = output + str.charAt(str.length() - 1);
        }*/

        if (output.length() > 0) {
            return output;
        } else {
            return "No Unique Element";
        }

    }


    public static void main(String[] args) {
        // write your code here
    }
}
