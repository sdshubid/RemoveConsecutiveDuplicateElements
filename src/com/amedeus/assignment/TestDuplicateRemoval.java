package com.amedeus.assignment;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestDuplicateRemoval {

    private RemoveConDuplicates removeConDuplicates;

    @Before
    public void setMain(){
        removeConDuplicates = new RemoveConDuplicates();
    }

    @Test
    public void removeDuplicatesUsingRecursion(){
        Assert.assertEquals("ade", removeConDuplicates.removeDuplicatesUsingRecursion("abccbde"));
        Assert.assertEquals("e",removeConDuplicates.removeDuplicatesUsingRecursion("abccbae"));
        Assert.assertEquals("No Unique Element", removeConDuplicates.removeDuplicatesUsingRecursion("bbccccbbb"));
    }


    @Test
    public void removeDuplicatesUsingLinearApproach(){
        Assert.assertEquals("ade", removeConDuplicates.removeDuplicatesLinearApproach("abccbde"));
        Assert.assertEquals("e",removeConDuplicates.removeDuplicatesLinearApproach("abccbae"));
        Assert.assertEquals("No Unique Element", removeConDuplicates.removeDuplicatesLinearApproach("bbccccbbb"));
    }
}
